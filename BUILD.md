# Der make-Befehl
In diesem Dokument steht, wie du dir auf deinem PC die Pdf Dateien selber erstellen kannst und was du noch so alles mit dem make Befehl machen kannst. Der make Befehl gefolgt von einem Kommando wird immer im Hauptordner des Repos ausgeführt. Um einen schnellen Überblick über alle Inhalte und mögliche Konfigurationen zu erhalten wird empfohlen folgenden Befehl auszuführen.

```
$ make release
```


## Make test
Mit dem nachfolgenden Befehl werden alle Beispiele gebuildet, welche in den letzten 60 Minuten bearbeitet wurden. Make small zeigt einen ausführlichen Errorlog in der Konsole an.

```
$ make test
```

## Make small
Mit make small wird von allen Buildbaren Inhalten genau eine Version mit Errorlog gebuildet.

```
$ make small
```

## Make release
Mit make release wird von allen Buildbaren Inhalten die 8 häufigsten open Versionen gebuildet. Ein mal pro Quartal wird dieser Befehl automatisch am Server ausgeführt und die Inhalte als [Release](https://gitlab.com/omn-dev-team/all-repos/-/releases) frei Zugänglich abgelegt.
```
make release
```

## Make all
Mit dem nachfolgenden Befehl werden alle möglichen Versionen von PDFs ohne Errorlog erstellt und in /conf/out abgelegt. Dieser Buildprozess kann, je nachdem wie umfangreich ein Repo ist, sehr lange dauern. Wenn du im vorhinein bereits weißt welche Version du möchtest, kannst du auch explizit nur bestimmte Versionen generieren. Siehe dazu **Selektiv erstellen.**

```
$ make all
```

## Selektiv erstellen
Der selektive make-Befehl besteht aus 8 Optionen, welche in der folgenden Reihenfolge, durch einen Bindestrich getrennt, aufgerufen werden.

```
$ make {opn,cld}-{exm,lab,skp,ue}-{bsp,mdl,mpc,thr,txt}-{ang,lsg,hyb}-{cmp,pge}-{one,two}-{max,avg}-{acin,emce,tele,esea}
```

### Erste Option
Welche Berechtigungsstufe soll gewählt werden?

```
opn		Open		Es werden nur Inhalte aus dem open Bereich kompiliert
cld		Closed		Es werden open und closed Inhalte kompiliert, Zugang zum geschlossenen Bereich vorausgesetzt
```

### Zweite Option
Aus welchem Bereich soll gewählt werden?

```
exm		Prüfung		Nur Inhalte von Prüfungen
lab		Labor		Nur Inhalte von Laboren
skp		Skriptum	Nur Inhalte von Skripten
ue		Übung		Nur Inhalte von Übungen
```

### Dritte Option
Welche Untergruppe soll gewählt werden?

```
bsp		Beispiel	Rechenbeispiele
mdl		Mündlich	Inhalte einer Mündlichen Prüfung
mpc		Mehrfachauswahl	Inhalte von Multiple Choice Fragen
thr		Theorie		Theoriefragen
txt		Text		Erklärender Text
```

### Vierte Option
Sollen nur Angaben, Lösungen oder beides angezeigt werden?

```
ang		Angabe		Ausschließlich die Angabe zu einer Frage
lsg		Lösung		Ausschließlich die Lösung zu einer Frage
hyb		Hybrid		Angabe und Lösung zu einer Frage
```

### Fünfte Option
Wie sollen die Aufgaben voneinander getrennt sein?

```
cmp		Kompakt		Fragen und Lösungen befinden sich unmittelbar nacheinander
pge		Seite		Jede Frage und/oder Lösung beginnt auf einer neuen Seite. z.B. um unterhalb eine eigene Lösung zu erarbeiten
```

### Sechste Option
Layout für einseitigen oder zweiseitigen Ausdruck? Wird ein einseitiges Dokument auf einem Drucker doppelseitig ausgedruckt, wird der Text beim Lochen oder Binden beschädigt oder verdeckt.

```
one		Einseitig	Das PDF ist für einseitigen Ausdruck optimiert
two		Zweiseitig	Das PDF ist für zweiseitigen Ausdruck optimiert
```

### Siebte Option
Wie soll die Seite platzmäßig ausgenützt werden?

```
max		Maximal		Die Seiten wird maximal ausgenützt, Abstände und Ränder werden minimiert
avg		Normal		Die Seiten wird laut Latex Standard-Definition ausgenützt
```

### Achte Option
Welches Style soll verwendet werden?

```
acin	Institut für Automatisierung	Vektoren werden fett geschrieben, die Einheitsmatrix als I bezeichnet
emce	Institut für Elektrotechnik		Vektoren werden mit einem Pfeil geschrieben, die Einheitsmatrix wird als E bezeichnet
tele	Institut für Telekommunikation	Vektoren werden mit einem Pfeil geschrieben, Stromdichte wird als S Bezeichnet
esea	Institut für Energietechnik		
```

## Ordnerstruktur anlegen
Wenn du selbst Inhalte hinzufügen möchtest, kannst du folgenden Befehl aufrufen. Dieser erstellt dir ein leeres Ordnerverzeichnis mit allen möglichen Kombinationen. Damit kannst du ganz bequem deine Dateien ablegen.

```
$ make file
```

## Aufräumen
Genau das Gegenteil von `make file` kann mit diesem Befehl erreicht werden. Es werden alle Ordner entfernt, welche keinen Inhalt haben. Ebenfalls werden Builddatein entfernt.

```
$ make clean
```

## Update
Diese Option wird nur von den Maintainern aufgerufen um die README, CONTRIBUTING, gitignore und andere allgemeine Dateien in den Repos zu aktualisieren. Du benötigst sie nicht.
